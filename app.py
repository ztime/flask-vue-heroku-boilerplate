from flask import Flask
from backend.backend import backend_app
from frontend.frontend import frontend_app

app = Flask(__name__)
app.register_blueprint(backend_app, url_prefix='/v1')
app.register_blueprint(frontend_app)
