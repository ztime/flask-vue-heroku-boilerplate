from flask import Blueprint, render_template

frontend_app = Blueprint('frontend_app', __name__,
    template_folder='templates',
    static_folder='static',
    static_url_path='/frontend/static'
)

@frontend_app.route("/")
def index():
    return render_template("index.html")
