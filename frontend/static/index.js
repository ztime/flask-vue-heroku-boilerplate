const api_endpoint = '/v1/'

const vm = new Vue({
	el: "#vm",
	delimiters: [ '[[', ']]' ],
	data: {
		greeting: 'Hellu, vue!',
		flask_greeting: ''
	},
	created: async function(){
		const gResponse = await fetch(api_endpoint + 'greeting');
		const gObject = await gResponse.json();
		this.flask_greeting = gObject.greeting;
	}
})
