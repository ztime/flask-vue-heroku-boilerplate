from flask import Blueprint

backend_app = Blueprint('backend_app', __name__)

@backend_app.route("/greeting")
def greeting():
    return {'greeting': 'Hellu from ya now!'}
