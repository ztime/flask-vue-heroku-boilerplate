# Boilerplate

Quick setup for prototyping containing:
* Backend (Flask)
* Frontend (Vue.js served through Flask)
* Ready for Heroku deploy with Procfile and requirements.txt using Gunicorn

## Deploy to Heroku

Sign up, create an account and create a new app

```
$heroku login
$heroku git:remote -a {app_name_here}
$heroku push heroku master
```
